Versy::Engine.routes.draw do
  post "/version/add", to: "versions#add", defaults: {format: 'text'}
  get "/versions/:id", to: "versions#list"
  get "/version/:id/:uid", to: "versions#get"
end

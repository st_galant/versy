require 'test_helper'

class VersyTest < ActiveSupport::TestCase
  test "truth" do
    assert_kind_of Module, Versy
  end

  test "config is exist" do
    assert ::Versy::CONFIG
  end

  test "config is from app" do
    assert_equal 'dummy', ::Versy::CONFIG[:config_name], ::Versy::CONFIG[:config_name]
  end
end

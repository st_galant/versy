require 'test_helper'

module Versy
  class RedisTest < ActiveSupport::TestCase
    test 'connection' do
      assert_equal 'PONG', ::Versy::Backend::Redis.redis.ping
    end
  end
end

require 'test_helper'

module Versy
  class StorageTest < ActiveSupport::TestCase
    test "save version and return its key" do
      version_key = ::Versy::Storage << @version
      refute version_key.empty? 
    end

    test "fetch object version by id and key" do
      version_uid = ::Versy::Storage << @version
      version = ::Versy::Storage.fetch id: @version.id, uid: version_uid

      assert_equal @version.id, version.id
      assert_equal @version.object, version.object
    end

    test "fetch list of versions by id" do
      random_id = srand.to_s
      time = Time.now
      uid = ::Versy::Storage << ::Versy::Version.new(id: random_id, object: "test_object", timestamp: time)
      versions = ::Versy::Storage.list id: random_id
      assert_equal versions.last.id, random_id, "uid: #{uid}"
      assert_equal versions.last.timestamp.to_f, time.to_f, "#{versions.last.timestamp.to_f} == #{time.to_f}"
    end
    
    test "fetch list of versions with paging" do
      random_id = srand.to_s
      uids = []
      (0..9).each do  |n|
        uids << (::Versy::Storage << ::Versy::Version.new(id: random_id, object: "object_#{n}", timestamp: Time.now))
      end
      offset = 5 
      limit = 5
      versions = ::Versy::Storage.list id: random_id, start: offset, limit: limit
      assert versions.count == 5, "count  = #{versions.count}"
      versions.each_index do |i|
        j = -(offset+i+1)
        #      assert_equal versions[i].u, "object_#{i+offset-1}"
        assert_equal versions[i].uid, uids[j], "#{i} , #{j}"
      end
    end

    test "should return versions count" do
      count_test_id = srand.to_s
      version = ::Versy::Version.new(:id => count_test_id, :object => @test_object)
      ::Versy::Storage << version
      count = ::Versy::Storage.count count_test_id
      assert_equal 1, count
    end
  end
end

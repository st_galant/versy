require 'test_helper'

class ::Versy::VersionTest < ActiveSupport::TestCase
 
  test "new version must have id" do
    assert_raises ::Versy::Version::ValidationError do
      ::Versy::Version.new(:object => @test_object)
    end
  end

  test "new version must have object" do
    assert_raises ::Versy::Version::ValidationError do
      ::Versy::Version.new(:id => @test_id)
    end
  end
  
  test "id must be not empty" do
    assert_raises ::Versy::Version::ValidationError do
      ::Versy::Version.new(:id => nil, :object => @test_object)
    end
    
    assert_raises ::Versy::Version::ValidationError do
      ::Versy::Version.new(:id => "", :object => @test_object)
    end
  end

  test "oject can be empty" do
    assert ::Versy::Version.new(:id => @test_id, :object => "")
    assert ::Versy::Version.new(:id => @test_id, :object => nil)
  end

  test "version object must be converted to string" do
    assert_respond_to @version.object, 'to_s'
  end

  test "version attributes" do
    tst = Time.now
    v = ::Versy::Version.new(:id => @test_id,
                    :object => @test_object,
                    :changed_by => @test_user,
                    :timestamp => tst,
                    :remote_host => "localhost",
                    :metadata => 'some usefull data'
                    )
    
    assert_equal @test_id, v.id, "id not equal"
    assert_equal @test_object, v.object.to_s, "object not equal"
    assert_equal @test_user, v.changed_by, "user not equal"
    assert_equal tst, v.timestamp, "timestamp not equal"
    assert_equal "localhost", v.remote_host, "host not equal"
    assert_equal 'some usefull data', v.metadata, "metadata not equal"
  end

end

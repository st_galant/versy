Rails.application.routes.draw do

  mount Versy::Engine => "/versy", defaults: { format: "text" }
end

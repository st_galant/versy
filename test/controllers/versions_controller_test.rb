require 'test_helper'
require 'json'
module Versy
  class VersionsControllerTest < ActionController::TestCase
    setup do
      @routes = Versy::Engine.routes
    end

    test "should add new version" do
      assert_difference('::Versy::Storage.count(@test_id)') do
        post :add, version: {id: @test_id, object: @test_object}
      end
    end

    test "should get object by id and uid" do
      test_uid = ::Versy::Storage << @version
      get :get, id: @test_id, uid: test_uid
      assert_response :success
    end

    test "should reeturn list of versions in text format" do
      ::Versy::Storage << @version
      get :list, id: @test_id, format: 'text'
      Rails.logger.debug @response.body
      assert_response :success
    end

    test "should reeturn list of versions in json format" do
      ::Versy::Storage << @version
      get :list, id: @test_id, format: 'json'
      Rails.logger.debug @response.body
      assert_response :success
    end

    test "should set timestap" do
      post :add, version: {id: @test_id, object: @test_object}
      get :list, id: @test_id, format: 'json'
      response = JSON.parse(@response.body)
      assert response["versions"][0]["timestamp"].to_datetime > 1.hour.ago
    end
  end
end

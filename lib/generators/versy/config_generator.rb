require 'rails/generators/base'
module Versy
  class ConfigGenerator < Rails::Generators::Base
    source_root File.expand_path('../../', __FILE__)
    def copy_config
      copy_file 'templates/versy.yml', 'config/versy.yml'
    end
  end
end

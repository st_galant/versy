require 'rails/generators/base'
module Versy
  class InstallGenerator < Rails::Generators::Base
    def add_route 
      route 'mount Versy::Engine => "/versy", defaults: { format: "text" }'
    end
  end
end

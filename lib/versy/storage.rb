require_relative "pipelines/fetch"
require_relative "pipelines/save"

module Versy
  class Storage
    def self.<< version
      version = ::Versy::Pipeline::Save.process(version)
      storage.save_new_version(version)
      return version.uid
    end
    def self.fetch id:, uid:
      version = storage.fetch_object(id, uid)
      version = ::Versy::Pipeline::Fetch.process(version)
    end

    def self.list id:, start: 0, limit: -1
      storage.get_list id, start, limit
    end

    def self.count id
      storage.get_versions_count id
    end
    
    class SaveError < StandardError
    end

    class FetchError < StandardError
    end

    class ArgumentError < StandardError
    end

    private_class_method :new
    private

    def self.storage
      @storage || set_storage
    end

    def self.set_storage
      storage_name = ::Versy::CONFIG[:storage][:adapter]
      #require_relative "backends/#{storage_name.underscore}"
      @storage = "::Versy::Backend::#{storage_name.camelize}".constantize
    end
  end
end

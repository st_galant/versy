module Versy
  DEFAULT_CONFIG =  {config_name: 'default',
                     storage: 
                           {adapter: 'redis',
                            host: 'localhost',
                            port: 6379},
                     compression: 'lz4',
                     expiration: 6.months.to_i,
                     info_expiration: 9.months.to_i,
                     uid_generation:
                           {method: 'sha256',
                            max_object_size: 4000000}
                    }
end

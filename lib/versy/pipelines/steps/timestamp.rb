module Versy
module Steps
  #Step of UID genetation
  class Timestamp
    def self.process version
      VersionWithTimestamp.new(version)
    end

    class VersionWithTimestamp
      extend Forwardable
      def_delegators :@version, *::Versy::Version::INTERFACE
      attr_reader :timestamp

      def initialize version
        @version = version
        @timestamp = version.timestamp || Time.now
      end
    end
  end
end
end

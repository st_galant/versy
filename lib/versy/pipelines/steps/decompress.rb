module Versy
module Steps
  class Decompress
    def self.process version
      DecompressedVersion.new(version)
    end

    #wrapper for version
    class DecompressedVersion
      extend Forwardable
      def_delegators :@version, *::Versy::Version::INTERFACE

      attr_reader :object

      def initialize version
        @version = version
        @object = ::Versy::Compressor.decompress(@version.object, @version.compression)
      end
    end
  end
end
end

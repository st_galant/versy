module Versy
module Steps
  #Step of UID genetation
  class GenerateUid
    def self.process version
      VersionWithUID.new(version)
    end

    class VersionWithUID
      extend Forwardable
      def_delegators :@version, *::Versy::Version::INTERFACE
      attr_reader :uid

      def initialize version
        @version = version
        @uid = ::Versy::UID.generate(version.object, 
                                   method: ::Versy::CONFIG[:uid_generation][:method],
                                   max_object_size: ::Versy::CONFIG[:uid_generation][:max_object_size])
      end
    end
  end
end
end

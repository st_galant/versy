module Versy
module Steps
  class Compress
    def self.process version
      CompressedVersion.new(version)
    end

    #wrapper for version
    class CompressedVersion
      extend Forwardable
      def_delegators :@version, *::Versy::Version::INTERFACE

      attr_reader :object, :compression

      def initialize version
        @version = version
        @compression = String(::Versy::CONFIG[:compression])
        @object = ::Versy::Compressor.compress(version.object, @compression)
      end
    end
  end
end
end

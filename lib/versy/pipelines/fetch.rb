require "potok"
require_relative "steps/decompress"

module Versy
  module Pipeline
    class Fetch < ::Potok::Pipe
      steps ::Versy::Steps::Decompress
    end
  end
end

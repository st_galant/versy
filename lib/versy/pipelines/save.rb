require "potok"
require_relative "steps/timestamp"
require_relative "steps/compress"
require_relative "steps/generate_uid"

module Versy
  module Pipeline
    class Save < ::Potok::Pipe
      steps ::Versy::Steps::Timestamp, ::Versy::Steps::Compress, ::Versy::Steps::GenerateUid
    end
  end
end

require 'lz4-ruby'

module Versy
  class Compressor

    def self.compress object, compressor_name = 'nop'
      codec(compressor_name).compress String(object)
    end

    def self.decompress object, compressor_name
      codec(compressor_name).decompress String(object)
    end

    def self.codec name
      begin
        "::Versy::Compressor::#{name.upcase}".constantize
      rescue NameError => err
        raise err, "wrong compression name: '#{name}' $config=#{$config} "
      end
    end

    class LZ4
      def self.compress object
        ::LZ4.compress object
      end

      def self.decompress object
        ::LZ4.decompress object
      end
    end

    class LZ4HC < LZ4
      def self.compress object
        ::LZ4.compressHC object
      end
    end

    class NOP
      def self.compress object
        object
      end
      def self.decompress object
        object
      end
    end
  end
end

module Versy
  class UID
    def self.generate  object, method: 'sha256', max_object_size: 1099511627776

      max_object_size = Integer(max_object_size)

      if method == 'fast' || object.bytesize > max_object_size
        generator = ->(obj){srand.to_s}
      elsif method == 'sha256'
        generator = ->(obj){::Digest::SHA256.hexdigest obj}
      else
        throw :unknown_key_generation_metod
      end
    generator.call(object)
    end
  end
end

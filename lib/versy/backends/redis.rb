require 'redis'

module Versy
  module Backend
    class Redis
      def self.save_new_version version
        version_info = {changed_by: version.changed_by,
          uid: version.uid,
          remote_host: version.remote_host, 
          timestamp: version.timestamp, 
        metadata: version.metadata}.to_json
        #save version info or exit
        unless redis.zadd(version.id, version.timestamp.to_f,  version_info)
          raise SaveError, "redis cannot add new version info"
        end
        redis.expire(version.id, ::Versy::CONFIG[:info_expiration]) if ::Versy::CONFIG[:info_expiration]
                #save object
        redis.setnx(version.id + redis_key_separator + version.uid, version.object)
        redis.expire(version.id + redis_key_separator + version.uid, ::Versy::CONFIG[:expiration]) if ::Versy::CONFIG[:expiration]

        #save compression information
        redis.setnx(version.id + redis_key_separator + version.uid + redis_key_separator + 'compression', version.compression)
        redis.expire(version.id + redis_key_separator + version.uid + redis_key_separator + 'compression', ::Versy::CONFIG[:expiration]) if ::Versy::CONFIG[:expiration]
      end

      def self.fetch_object id, uid
        object = redis.get(id + redis_key_separator + uid)
        unless object
          raise FetchError, "redis cannot find object #{id}::#{uid}"
        end
        compression = redis.get(id + redis_key_separator + uid + redis_key_separator + 'compression')
                return Struct.new(:id, :object, :compression).new(id, object, compression)
      end

      def self.get_versions_count id
        redis.zcount id, "-inf", "+inf" 
      end

      def self.get_list id, start, limit
        list = []
        if limit < 0
          stop = limit
        else
          stop = start + limit - 1
        end
        result = redis.zrevrange(id, start, stop, :with_scores => true)
        result.each do |v|
          version_info = JSON.parse(v[0]).symbolize_keys! #{digest: object_digest, changed_by: user_id, remote_host: ip}
          version_info.merge!({id: id, timestamp: Time.at(v[1])})
          list << VersionInfo.new(version_info)
        end
        return list
      end

      def self.get_versions_count id
        redis.zcount id, "-inf", "+inf"
      end

      def self.redis
        @@redis ||= ::Redis.new(:host => ::Versy::CONFIG[:storage][:host], :port => ::Versy::CONFIG[:storage][:port], :db => ::Versy::CONFIG[:storage].fetch(:db){ 1 })
      end

      def self.redis_key_separator
        @key_separator ||= ::Versy::CONFIG[:storage].fetch(:key_separator){'::'}
      end
    end

    class VersionInfo
      extend Forwardable
      def_delegators :@version, *::Versy::Version::INTERFACE
      attr_reader :uid

      def initialize options
        options.merge!({object: ""})
        @version = Version.new options
        @uid = options.fetch(:uid) {throw :no_uid_in_stored_version_info}
      end

      def object
        raise NoMethodError, "object in versions list element is not defined (lazy load feature is possible, but not realised)"
        #lazy load возможен, но, скорее всего, не нужен
        #@object ||= VersionsStorage.fetch id, uid
      end
    end
  end
end

module Versy
  class Version
    INTERFACE = [:id, :object, :changed_by, :timestamp, :remote_host, :metadata, :uid, :compression]
    attr_reader *INTERFACE

    def initialize options
      @id = String(options[:id])
      raise ValidationError if @id.empty?
            @object = options.fetch(:object){raise ValidationError}
      @changed_by = options[:changed_by]
      @timestamp = options[:timestamp]
      @remote_host = options[:remote_host]
      @metadata = options[:metadata]
    end

    class ValidationError < StandardError
    end
  end
end

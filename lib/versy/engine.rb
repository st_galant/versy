require_relative 'default_config'

module Versy
  class Engine < ::Rails::Engine
    isolate_namespace Versy

    initializer 'versy.config' do |app|
      config =  begin 
                  Rails.application.config_for(:versy).deep_symbolize_keys
                rescue
                  ::Versy::DEFAULT_CONFIG
                end

      ::Versy::CONFIG = config

      backend_file =  File.expand_path("../backends/#{::Versy::CONFIG[:storage][:adapter]}.rb", __FILE__)
      if File.exist?(backend_file)
        require backend_file
      end



    end
  end
end

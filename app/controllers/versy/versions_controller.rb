module Versy
  class VersionsController < ApplicationController
    def add
      time = Time.now
      charset = request.headers.fetch('Content-Charset'){"utf-8"}
      version_info = add_params.merge({timestamp: time,
                                       remote_host: request.remote_ip,
                                       charset: charset
                                      }).symbolize_keys
      
      #TODO implement object as a file 
      if version_info[:object].is_a? ActionDispatch::Http::UploadedFile
        render nothing: true, status: :not_implemented
      end

      begin
        version = ::Versy::Version.new version_info
        uid = ::Versy::Storage << version
      rescue ::Versy::Version::ValidationError
        render nothing: true, status: :bad_request
      rescue ::Versy::Storage::SaveError
        render nothing: true, status: :internal_server_error
      end
      render plain: uid, layout: false, status: :ok
    end

    def get
      begin
        object, charset = ::Versy::Storage.fetch get_params.symbolize_keys
      rescue ::Versy::Storage::FetchError
        render nothing: true, status: :not_found
      end
      response.charset = charset || request.headers['Accept-Charset'] || 'utf-8'
      render plain: object, layout: false, content_type: 'text/plain', status: :ok
    end

    def list
      begin
        p = list_params
        @id = p[:id]

        @list = Storage.list p
      rescue Storage::ArgumentError
        render nothing: true, status: :bad_request
      end
            charset = request.headers.fetch('Accept-charset'){"utf-8"}
      response.charset = charset
      #byebug 
      respond_to do |format|
        format.text
        format.json
      end
    end

    private
    def add_params
      params.require(:version).permit(:id, :object, :changed_by, :metadata)
    end

    def get_params
      params.permit(:id,:uid)
    end

    def list_params
      p = params.permit(:id, :start, :limit).symbolize_keys
      p[:start] = Integer(p.fetch(:start){0})
      p[:limit] = Integer(p.fetch(:limit){-1})
      return p
    end
  end
end

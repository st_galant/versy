json.id @id
json.versions @list do |l|
  json.timestamp    l.timestamp.strftime("%d.%m.%Y %T")
  json.changed_by   l.changed_by
  json.remote_host  l.remote_host
  json.uid          l.uid
end

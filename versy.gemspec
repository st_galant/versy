$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "versy/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "versy"
  s.version     = Versy::VERSION
  s.authors     = ["st"]
  s.email       = ["st.galant@gmail.com"]
  s.homepage    = "https://bitbucket.org/st_galant/versy"
  s.summary     = "Engine for versioning objects"
  s.description = "Engine for versioning objects"
  s.license     = "MIT"

  s.files = Dir["{app,config,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.5"
  s.add_dependency 'jbuilder', '~> 2.0'
  s.add_dependency "potok"
  s.add_dependency "lz4-ruby"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "redis"
end
